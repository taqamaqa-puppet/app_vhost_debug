# frozen_string_literal: true

require 'spec_helper'

describe 'app_vhost' do
  let(:title) { 'app_vhost' }

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      let(:pre_condition) do
        'class { \'apache\':
          default_vhost    => false,
          server_signature => off,
        }'
      end

      context 'with vhostname as FQDN' do
        let(:params) do
          {
            vhost_name: 'example.org',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_apache__vhost('example.org') }
        it { is_expected.to contain_file('/var/www-vhosts/example.org') }
      end
    end
  end
end
