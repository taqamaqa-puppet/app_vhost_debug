require 'spec_helper_acceptance'

describe 'app_vhost' do
  context 'with vhost_name param' do
    pp = %(
      class { 'app_vhost' :
        vhost_name => 'example.org',
      }
    )

    it 'creates vhost without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'creates vhost idempotently' do
      apply_manifest(pp, catch_changes: true)
    end
  end
end
