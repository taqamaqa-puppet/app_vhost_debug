# @summary Manage an Apache Vhost
#
# Manage an Apache Vhost
#
# @param vhost_name Name of the vhost (FQDN ou IP address)
#
# @example
#   include app_vhost
class app_vhost (
  Stdlib::Host $vhost_name
) {
  apache::vhost { $vhost_name:
    servername    => $vhost_name,
    serveraliases => "www.${vhost_name}",
    port          => '80',
    docroot       => "/var/www-vhosts/${vhost_name}",
    docroot_owner => 'www-data',
    docroot_group => 'www-data',
    docroot_mode  => '755',
  }
}
